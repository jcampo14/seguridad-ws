package com.bussinessolutions.application.security;

public class Constants {

	// Spring Security
	public static final String LOGIN_URL = "/auth";
	public static final String HEADER_AUTHORIZACION_KEY = "Authorization";
	public static final String TOKEN_BEARER_PREFIX = "Bearer ";

	// JWT
	public static final String ISSUER_INFO = "https://www.autentia.com/";
	public static final String SECRET_KEY = "S3CUR1TY_4PP_BS";
	public static final long TOKEN_EXPIRATION_TIME = 86_400_000; // 1 day

}
