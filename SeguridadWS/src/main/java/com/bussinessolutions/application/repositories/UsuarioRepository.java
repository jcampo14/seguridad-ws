package com.bussinessolutions.application.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.bussinessolutions.application.model.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Long>{

	@Query("FROM Usuario WHERE username = :pUsername")
	public Usuario findByUsername(@Param("pUsername") String username);
}
