package com.bussinessolutions.application.services;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.bussinessolutions.application.model.Usuario;
import com.bussinessolutions.application.repositories.UsuarioRepository;

@Service
public class UsuarioServices implements UserDetailsService {

	@Autowired
	private UsuarioRepository repository;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	

	public List<Usuario> findAll() {
		return (List<Usuario>) repository.findAll();
	}

	public Usuario findByUsername(String username) {
		return repository.findByUsername(username);
	}

	public void create(Usuario entity) {
		entity.setPassword(bCryptPasswordEncoder.encode(entity.getPassword()));
		repository.save(entity);
	}

	public boolean update(Usuario entity) {
		Usuario exist = repository.findOne(entity.getId());
		if (exist != null) {			
			entity.setPassword(bCryptPasswordEncoder.encode(entity.getPassword()));
			repository.save(entity);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Usuario usuario = repository.findByUsername(username);
		if (usuario == null) {
			throw new UsernameNotFoundException(username);
		}		
		return new User(usuario.getUsername(), usuario.getPassword(), Collections.emptyList());
	}
}
