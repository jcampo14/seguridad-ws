package com.bussinessolutions.application.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bussinessolutions.application.model.Usuario;
import com.bussinessolutions.application.services.UsuarioServices;

@RestController
public class UsuarioController {

	private static final String SERVICE_PATH = "/usuarios";
	
	@Autowired
	private UsuarioServices services;		
	
	@RequestMapping(method = RequestMethod.GET, path = SERVICE_PATH)
	public List<Usuario> findAll(){
		return services.findAll();
	}
	
	@RequestMapping(method = RequestMethod.POST, path = SERVICE_PATH)
	public boolean create(@RequestBody Usuario body) {		
		services.create(body);
		return true;
	}
	
	@RequestMapping(method = RequestMethod.PUT, path = SERVICE_PATH)
	public boolean update(@RequestBody Usuario body) {		
		return services.update(body);		
	}
	
}
